#[allow(warnings)]
fn run(exe_path:&str,url:&str) -> Result<(), Box<dyn std::error::Error>> {
    use futures::StreamExt;
    use chromiumoxide::{Browser, BrowserConfig};
    use async_std::task;
    task::block_on(async {
        let browser_config = BrowserConfig::with_executable(exe_path);
        let (mut browser, mut handler) = Browser::launch(browser_config).await?;
        let handle = async_std::task::spawn(async move {
                let _event = handler.next().await.unwrap();
        });
        let page = browser.new_page(url).await?;
        let html = page.wait_for_navigation().await?.content().await?;
        println!("{}", html);
        Ok(())
    })
}
#[allow(warnings)]
// #[cfg(target_os = "linux")]
fn main() {
    // browser path: "/usr/bin/chromium"
    // in macos : "/Applications/Chromium.app/Contents/MacOS/Chromium"
    // url : ""
    if std::env::args().skip(1).len()>1{
        let exe_path = std::env::args().skip(1).nth(0).unwrap();
        let url = std::env::args().skip(1).nth(1).unwrap();
        if let Err(err) = run("/Applications/Chromium.app/Contents/MacOS/Chromium",&url) {
            eprintln!("An error occurred: {:?}", err);
        }
    }else {
        eprintln!("Just Need browser exe path and url argument");
        eprintln!("Example:");
        eprintln!("puppeteer_cli <browser_path> <url>");
        eprintln!("puppeteer_cli /usr/bin/chromium https://www.hackingwithswift.com");
    }
}
