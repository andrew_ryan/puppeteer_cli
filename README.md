# puppeteer cli
[![Crates.io](https://img.shields.io/crates/v/puppeteer_cli.svg)](https://crates.io/crates/puppeteer_cli)
[![Rust](https://img.shields.io/badge/rust-1.56.1%2B-blue.svg?maxAge=3600)](https://gitlab.com/andrew_ryan/puppeteer_cli)
[![license](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/andrew_ryan/puppeteer_cli/-/raw/master/LICENSE)
## Example:
```sh
puppeteer_cli <browser_path> <url>
puppeteer_cli /usr/bin/chromium https://www.hackingwithswift.com
```
